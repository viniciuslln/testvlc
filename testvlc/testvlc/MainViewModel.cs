﻿using LibVLCSharp.Shared;
using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace testvlc
{
    class MainViewModel : BaseViewModel
    {
        private LibVLC libVLC;
        private MediaPlayer _mediaPlayer;

        public LibVLC LibVLC { get => libVLC; set => SetProperty(ref libVLC, value); }
        public MediaPlayer MediaPlayer { get => _mediaPlayer; set => SetProperty(ref _mediaPlayer, value); }

        public void ConfigurePlayer()
        {
            if (LibVLC == null)
            {
                LibVLC = new LibVLC(null);
                var Media = new Media(LibVLC, "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4", FromType.FromLocation);

                MediaPlayer = new MediaPlayer(Media);
                var startPlay = MediaPlayer.Play();
            }
        }

    }
}
